/*
 * DicomDB: org.nrg.dcm.AttrDefs
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dcm;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public interface AttrDefs extends org.nrg.attr.AttrDefs<DicomAttributeIndex> {}
