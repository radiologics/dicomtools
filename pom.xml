<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ dicomtools: pom.xml
  ~ XNAT https://www.xnat.org
  ~ Copyright (c) 2020, Washington University School of Medicine
  ~ All Rights Reserved
  ~
  ~ Released under the Simplified BSD.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.nrg</groupId>
        <artifactId>parent</artifactId>
        <version>1.7.7-INTERNAL</version>
    </parent>

    <artifactId>dicomtools</artifactId>

    <name>NRG DICOM Tools</name>
    <description>Provides a high-level set of tools for managing and manipulating DICOM data.</description>

    <scm>
        <url>https://bitbucket.org/xnatdev/dicomtools</url>
        <connection>scm:git:git://bitbucket.org/xnatdev/dicomtools.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:xnatdev/dicomtools.git</developerConnection>
    </scm>

    <issueManagement>
        <system>JIRA</system>
        <url>https://issues.xnat.org</url>
    </issueManagement>

    <licenses>
        <license>
            <name>Simplified BSD License</name>
            <url>https://www.opensource.org/licenses/BSD-2-Clause</url>
        </license>
    </licenses>

    <organization>
        <name>Neuroinformatics Research Group</name>
        <url>https://nrg.wustl.edu</url>
    </organization>

    <dependencies>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>config</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>*</groupId>
                    <artifactId>*</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>framework</artifactId>
        </dependency>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>ExtAttr</artifactId>
        </dependency>
        <dependency>
            <groupId>dcm4che</groupId>
            <artifactId>dcm4che-core</artifactId>
        </dependency>
        <dependency>
            <groupId>dcm4che</groupId>
            <artifactId>dcm4che-net</artifactId>
        </dependency>
        <dependency>
            <groupId>dcm4che</groupId>
            <artifactId>dcm4che-iod</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hsqldb</groupId>
            <artifactId>hsqldb</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>jsr305</artifactId>
        </dependency>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>automation</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.codehaus.groovy</groupId>
                    <artifactId>groovy-all</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.python</groupId>
                    <artifactId>jython-standalone</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>javax.inject</groupId>
            <artifactId>javax.inject</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.ant</groupId>
            <artifactId>ant</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>commons-beanutils</groupId>
            <artifactId>commons-beanutils</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-envers</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-ehcache</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-source-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-javadoc-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>org.nrg.maven.artifacts.release</id>
            <name>XNAT Release Maven Repo</name>
            <url>https://nrgxnat.jfrog.io/nrgxnat/libs-release</url>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>fail</checksumPolicy>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>org.nrg.maven.artifacts.snapshot</id>
            <name>xnat snapshot maven repo</name>
            <url>https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
                <checksumPolicy>fail</checksumPolicy>
            </snapshots>
        </repository>
    </repositories>

</project>
